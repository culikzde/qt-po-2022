#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->input->setText
(
R"CODE(
button.text = "abc";
output.text = "abc";
sum = 0;
for (i = 1; i < 10 ; i++)
{
  sum += i;
}
sum)CODE"
);

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_actionRun_triggered()

{
    QJSEngine myEngine;
    QJSValue proxy;

    proxy = myEngine.newQObject (ui->pushButton);
    myEngine.globalObject().setProperty ("button", proxy);

    proxy = myEngine.newQObject (ui->output);
    myEngine.globalObject().setProperty ("output", proxy);

    QJSValue three = myEngine.evaluate (ui->input->toPlainText());
    ui->output->append(three.toString());
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();
    return a.exec();
}

