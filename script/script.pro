QT       += core gui widgets qml

# dnf install qt5-qtquickcontrols2-devel

CONFIG += c++11 precompile_header

PRECOMPILED_HEADER += precompiled.h

SOURCES += \
    mainwindow.cpp

HEADERS += \
    precompiled.h \
    mainwindow.h

FORMS += \
    mainwindow.ui

