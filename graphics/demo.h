#ifndef DEMO_H
#define DEMO_H

#include <QMainWindow>
#include <QGraphicsScene>
#include <QTreeWidgetItem>
#include <QTableWidgetItem>

#include "scene.h"
#include "win.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow, public Win
{
    Q_OBJECT

public:
    MainWindow (QWidget * parent = nullptr);
    ~MainWindow ();

    void loadFile(QString fileName, bool json = false);
    void saveFile (QString fileName, bool json = false);

    void refreshTree () override;
    void refreshProperties (QGraphicsItem * item) override;
    void refreshTreeName (QGraphicsItem * item, QString name) override;
    void refreshNewTreeItem (QGraphicsItem * item) override;

 private:
    void addView (QString title, QWidget * widget);


private slots:
    void on_actionOpen_triggered ();
    void on_actionSave_triggered ();

    void on_actionClear_triggered();
    void on_actionRun_triggered ();
    void on_actionQuit_triggered ();

    void on_actionCopy_triggered();
    void on_actionPaste_triggered();

    void on_tree_itemSelectionChanged();
    void on_tree_itemDoubleClicked (QTreeWidgetItem *item, int column);

    void on_actionChart_triggered();
    void on_actionPieChart_triggered();
    void on_actionVisualisation_triggered();

    void on_actionDatabase_triggered();
    void on_actionSVG_triggered();
    void on_actionHTML_triggered();

    void on_actionHTML_Local_Socket_triggered();

private:
    Ui::MainWindow * ui;

    // QGraphicsScene * scene;
    Scene * scene;
};

#endif // DEMO_H
