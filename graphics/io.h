#ifndef DROP_H
#define DROP_H

#include <QString>
#include <QGraphicsItem>

const QString toolFormat = "application/x-tool";
const QString shapeFormat = "application/x-shape";

const int opaqueKey = 0;

QString penToString (QPen p);
QString brushToString (QBrush b);

QGraphicsItem * createItem (QString type);
void setupItem (QGraphicsItem * item);

QString itemType (QGraphicsItem * item);

QGraphicsItem * itemFromTool (QString tool, int x, int y);

#endif // DROP_H
