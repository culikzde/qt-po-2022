#include "scene.h"
#include "io.h"

#include <QAbstractGraphicsShapeItem>
#include <QGraphicsSceneDragDropEvent>
#include <QMimeData>

// #include <iostream>
// using namespace std;

/* ---------------------------------------------------------------------- */

/*
View::View (QWidget * parent) :
    QGraphicsView (parent)
{
}

View::~View ()
{
}
*/

/* ---------------------------------------------------------------------- */

Scene::Scene () :
   win (nullptr)
{
    connect (this, &QGraphicsScene::selectionChanged, this, &Scene::onSelectionChanged);
}

void Scene::onSelectionChanged ()
{
    QList <QGraphicsItem * > list = selectedItems ();
    if (list.count () == 1)
       win->refreshProperties (list [0]);
    else
       win->refreshProperties (nullptr);
}

void Scene::dragEnterEvent (QGraphicsSceneDragDropEvent * event)
{
    const QMimeData * mimeData = event->mimeData();
    if (mimeData->hasColor() || mimeData->hasFormat (shapeFormat))
    {
        event->setAccepted (true);
    }
}

// see http://stackoverflow.com/questions/4177720/accepting-drops-on-a-qgraphicsscene
void Scene::dragMoveEvent (QGraphicsSceneDragDropEvent *event)
{
    const QMimeData * mimeData = event->mimeData();
    if (mimeData->hasColor() || mimeData->hasFormat (toolFormat) || mimeData->hasFormat (shapeFormat))
    {
        event->setAccepted (true);
    }
}

void Scene::dropEvent (QGraphicsSceneDragDropEvent * event)
{
    const QMimeData * mimeData = event->mimeData();
    if (mimeData->hasColor())
    {
           QColor color =  mimeData->colorData().value <QColor> ();
           // addLine (0, 0, 100, 200, color);

           QGraphicsItem * item = itemAt (event->scenePos (), QTransform ()); // do not use event->pos()
           QAbstractGraphicsShapeItem * shape = dynamic_cast < QAbstractGraphicsShapeItem * > (item);

           if (shape != nullptr)
           {
               if (event->proposedAction() == Qt::CopyAction) /* ctrl */
                  shape->setPen (color);
               else
                  shape->setBrush (color);
           }
    }
    if (mimeData->hasFormat (toolFormat))
    {
        QString tool = mimeData->data (toolFormat);
        QPointF p = event->scenePos ();
        int x = int (p.x ());
        int y = int (p.y ());
        QGraphicsItem * item = itemFromTool (tool, x, y);

        QGraphicsItem * target = itemAt (p, QTransform ());
        if (target == nullptr)
           addItem (target);
        else
           item->setParentItem (target);
        win->refreshTree ();
    }
    if (mimeData->hasFormat (shapeFormat))
    {
        QString code = mimeData->data (shapeFormat);
        // if (global_win != NULL)
        //   global_win->addGraphicsItem (shape, code, event->proposedAction() == Qt::MoveAction);
    }
}

/* ---------------------------------------------------------------------- */
