
/* svg.cc */

#include "svg.h"

/* ---------------------------------------------------------------------- */

SvgView::SvgView (QWidget * parent) :
   QSvgWidget (parent)
{
   load (QString ("/home/zdenek/Downloads/tiger.svg"));
   // load (QString ("/home/zdenek/Downloads/grapes.svg"));

   QNetworkAccessManager * manager = new QNetworkAccessManager (this);
   QObject::connect (manager, SIGNAL (finished (QNetworkReply*)),
                     this,     SLOT  (replyFinished (QNetworkReply*)) );

   QNetworkRequest request;
   request.setUrl (QUrl ("https://dev.w3.org/SVG/tools/svgweb/samples/svg-files/tiger.svg"));
   QNetworkReply * reply = manager->get (request);
}

void SvgView::replyFinished (QNetworkReply * reply)
{
   QByteArray answer = reply->readAll ();
   this->load (answer);
}

/* ---------------------------------------------------------------------- */


