#include "io.h"

#include <QGraphicsRectItem>
#include <QGraphicsEllipseItem>
#include <QGraphicsLineItem>
#include <QPen>

/* ---------------------------------------------------------------------- */

QString penToString (QPen p)
{
    return p.color().name();
}

QString brushToString (QBrush b)
{
    return b.color().name();
}

/* ---------------------------------------------------------------------- */

QString itemType (QGraphicsItem * item)
{
    QString result = "node";
    if (dynamic_cast < QGraphicsRectItem * > (item) != nullptr)
       result = "rectangle";
    if (dynamic_cast < QGraphicsEllipseItem * > (item) != nullptr)
       result = "ellipse";
    if (dynamic_cast < QGraphicsLineItem * > (item) != nullptr)
       result = "line";

    return result;
}

/* ---------------------------------------------------------------------- */

QGraphicsItem * createItem (QString type)
{
    QGraphicsItem * result = nullptr;

    if (type == "rectangle")
        result = new QGraphicsRectItem;
    else if (type == "ellipse")
        result = new QGraphicsEllipseItem;
    else if (type == "line")
        result = new QGraphicsLineItem;

    return result;
}

/* ---------------------------------------------------------------------- */

void setupItem (QGraphicsItem * item)
{
    item->setFlag (QGraphicsItem::ItemIsMovable);
    item->setFlag (QGraphicsItem::ItemIsSelectable);
}

/* ---------------------------------------------------------------------- */

QGraphicsItem * itemFromTool (QString tool, int x, int y)
{
    QGraphicsItem * result = nullptr;

    if (tool == "rectangle")
    {
        QGraphicsRectItem * item = new QGraphicsRectItem;
        item->setRect (0, 0, 100, 80);
        item->setPen (QColor ("blue"));
        item->setBrush (QColor ("yellow"));
        item->setToolTip ("rectangle");
        item->setFlags (QGraphicsItem::ItemIsMovable | QGraphicsEllipseItem::ItemIsSelectable);
        result = item;
    }
    else if (tool == "ellipse")
    {
        QGraphicsEllipseItem * item = new QGraphicsEllipseItem;
        item->setRect (0, 0, 60, 40);
        item->setPen (QColor ("orange"));
        item->setBrush (QColor ("lime"));
        item->setToolTip ("ellipse");
        result = item;
    }
    else if (tool == "line")
    {
        QGraphicsLineItem * item = new QGraphicsLineItem;
        item->setLine (0, 0, 60, 40);
        item->setPen (QColor ("red"));
        item->setToolTip ("line");
        result = item;
    }

    if (result != nullptr)
    {
       result->setPos (x, y);
       result->setFlags (QGraphicsItem::ItemIsMovable  | QGraphicsEllipseItem::ItemIsSelectable);
    }
    return result;
}

/* ---------------------------------------------------------------------- */
