#include "tree.h"
#include "io.h"
#include "demo.h"

#include <QPen>
#include <QAbstractGraphicsShapeItem>

// #include <iostream>
// using namespace std;

/* ---------------------------------------------------------------------- */

Tree::Tree (QWidget * parent) :
    QTreeWidget (parent),
    scene (nullptr),
    win (nullptr)
{
    setAcceptDrops (true); // <-- important
    setDropIndicatorShown (true);
    // setDragDropMode(InternalMove);
}

/* ---------------------------------------------------------------------- */

QStringList Tree::mimeTypes() const // <-- importatnt
{
    return QStringList () << "application/x-color" << toolFormat;
}

Qt::DropActions Tree::supportedDropActions() const
{
   return Qt::MoveAction | Qt::CopyAction  | Qt::LinkAction;
}

bool Tree::dropMimeData (QTreeWidgetItem * target, int index, const QMimeData * data, Qt::DropAction action)
{
    bool result = false;
    TreeNode * node = dynamic_cast < TreeNode * > (target);

    if (data->hasColor ())
    {
        QColor color = data->colorData().value <QColor> ();
        if (node != nullptr)
        {
           QAbstractGraphicsShapeItem * shape = dynamic_cast < QAbstractGraphicsShapeItem * > (node->item);
           if (shape != nullptr)
           {
               if (action == Qt::CopyAction) /* ctrl */
                  shape->setPen (color); // need #include <QPen>
               else
                  shape->setBrush (color);
           }
        }
        result = true;
    }
    else if (data->hasFormat (toolFormat))
    {
        QString tool = data->data (toolFormat);
        QGraphicsItem * item = itemFromTool (tool, 0, 0);
        if (item != nullptr)
        {
           if (node == nullptr)
               scene->addItem (item);
           else
               item->setParentItem (node->item);
        }
        win->refreshNewTreeItem (item);
    }

    return result;
}

/* ---------------------------------------------------------------------- */

void Tree::displayBranch (QTreeWidgetItem * target, QGraphicsItem * item)
{
    TreeNode * node = new TreeNode;
    node->setText (0, item->toolTip ());
    node->item = item;

    bool opaque = item->data (opaqueKey).toBool ();
    if (not opaque)
       for (QGraphicsItem * t : item->childItems ())
          displayBranch (node, t);

    if (target == nullptr)
       addTopLevelItem (node);
    else
       target->addChild (node);
}

void Tree::displayTree ()
{
    clear();

    for (QGraphicsItem * item : scene->items (Qt::AscendingOrder))
    {
        if (item->parentItem () == nullptr)
            displayBranch (nullptr, item);
    }

    expandAll();
}

/* ---------------------------------------------------------------------- */

TreeNode * Tree::findTreeItem (QGraphicsItem * item)
{
    TreeNode * result = nullptr;
    if (item != nullptr)
    {
        QGraphicsItem * above = item->parentItem ();
        QTreeWidgetItem * branch = nullptr;
        if (above == nullptr)
        {
            branch = invisibleRootItem ();
        }
        else
        {
            branch = findTreeItem (above);
        }
        int cnt = branch->childCount ();
        for (int inx = 0; inx < cnt && result == nullptr; inx ++)
        {
           TreeNode * node = dynamic_cast <TreeNode *> (branch->child (inx));
           if (node != nullptr && node->item == item)
              result = node;
        }
    }
    return result;
}

void Tree::renameTreeItem (QGraphicsItem * item, QString name)
{
    /*
    QTreeWidgetItemIterator it (this);
    while (*it)
    {
       TreeNode * node = dynamic_cast < TreeNode * > (*it);
       if (node != nullptr && node->item == item)
       {
           node->setText (0, name);
       }
       it ++;
    }
    */
    TreeNode * node = findTreeItem (item);
    if (node != nullptr)
        node->setText (0, name);
}

void Tree::addTreeItem (QGraphicsItem * item)
{
    QGraphicsItem * above = item->parentItem ();
    QTreeWidgetItem * branch = nullptr;
    if (above == nullptr)
        branch = invisibleRootItem ();
    else
        branch = findTreeItem (above);

    TreeNode * node = new TreeNode ();
    node->item = item;
    node->setText (0, item->toolTip ());
    branch->addChild (node);
}

/* ---------------------------------------------------------------------- */

