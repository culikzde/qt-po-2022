#include "property.h"

#include <QPen>
#include <QColorDialog>
#include <QVariant>

#include <QHeaderView>
#include <QVBoxLayout>

/* ---------------------------------------------------------------------- */

PropertyTable::PropertyTable (QWidget * parent) :
    QTableWidget (parent),
    win (nullptr),
    graphics_item (nullptr)
{
    setColumnCount(2);

    QStringList labels;
    labels << "Name" << "Value";
    setHorizontalHeaderLabels (labels);

    horizontalHeader ()->setStretchLastSection (true);

    setItemDelegate (new CustomDelegate (this));
    setEditTriggers (QAbstractItemView::AllEditTriggers);

    connect (this, &QTableWidget::itemChanged, this, &PropertyTable::onItemChanged);
}

/* ---------------------------------------------------------------------- */

QTableWidgetItem * PropertyTable::addTableLine (QString name, QString value)
{
    int cnt = rowCount();
    setRowCount(cnt+1);

    QTableWidgetItem * cell = new QTableWidgetItem;
    cell->setText (name);
    setItem (cnt, 0, cell);

    cell = new QTableWidgetItem;
    cell->setText (value);
    setItem (cnt, 1, cell);

    return cell;
}

void PropertyTable::addBool (QString name, bool value)
{
    QTableWidgetItem * cell = addTableLine (name, "" /* value ? "true" : "false" */);
    cell->setData (Qt::EditRole, value);
    // cell->setFlags (cell->flags () | Qt::ItemIsUserCheckable);
    // cell->setData (Qt::CheckStateRole, value ? Qt::Checked : Qt::Unchecked);
    // cell->setData (Qt::UserRole, "boolean");
}

void PropertyTable::addNum (QString name, int value)
{
    QTableWidgetItem * cell = addTableLine (name, "" /* QString::number (value) */);
    cell->setData (Qt::EditRole, value);
}

void PropertyTable::addReal (QString name, double value)
{
    QTableWidgetItem * cell = addTableLine (name, "" /* QString::number (value) */);
    cell->setData (Qt::EditRole, value);
}

void PropertyTable::addText (QString name, int value)
{
    QTableWidgetItem * cell = addTableLine (name, "" /* QString::number (value) */);
    cell->setData (Qt::EditRole, value);
}

void PropertyTable::addColor (QString name, QColor value)
{
    QTableWidgetItem * cell = addTableLine (name, "" /* value.name() */);
    cell->setData (Qt::EditRole, value);
    // cell->setData (Qt::DecorationRole, value);
}

void PropertyTable::addList (QString name, QStringList value)
{
    QTableWidgetItem * cell = addTableLine (name, "");
    cell->setData (Qt::EditRole, value);
}

/* ---------------------------------------------------------------------- */

void PropertyTable::displayProperties (QGraphicsItem * item)
{
    graphics_item = item;
    setRowCount (0);
    if (item != nullptr)
    {
        addTableLine ("tooltip", item->toolTip());
        addNum ("x", item->x());
        addNum ("y", item->y());

        if (QAbstractGraphicsShapeItem * shape = dynamic_cast < QAbstractGraphicsShapeItem * > (item))
        {
            addColor ("pen", shape->pen().color() );
            addColor ("brush", shape->brush().color() );

            if (QGraphicsRectItem * r = dynamic_cast < QGraphicsRectItem * > (shape))
            {
               addNum ("width", r->rect().width() );
               addNum ("height", r->rect().height() );
            }

            if (QGraphicsEllipseItem * e = dynamic_cast < QGraphicsEllipseItem * > (shape))
            {
               addNum ("width", e->rect().width() );
               addNum ("height", e->rect().height() );
            }
        }

        if (QGraphicsLineItem * e = dynamic_cast < QGraphicsLineItem * > (item))
        {
           addColor ("pen", e->pen().color() );
           addNum ("width", e->line().dx() );
           addNum ("height", e->line().dy() );
        }

        addBool ("boolean", true);
        addReal ("real", 3.14);
        addList ("list", QStringList () << "abc" << "def" << "klm");
    }
}

/* ---------------------------------------------------------------------- */

void PropertyTable::storeProperty (QGraphicsItem * item, QString name, QVariant value)
{
    if (name == "tooltip") item->setToolTip (value.toString ());
    if (name == "x") item->setX (value.toInt ());
    if (name == "y") item->setY (value.toInt ());

    if (QAbstractGraphicsShapeItem * shape = dynamic_cast < QAbstractGraphicsShapeItem * > (item))
    {
        if (name == "pen")
        {
           QColor color = value.value <QColor> ();
           shape->setPen (color);
        }

        if (name == "brush")
        {
            QColor color = value.value <QColor> ();
            shape->setBrush (color);
        }

        if (QGraphicsRectItem * e = dynamic_cast < QGraphicsRectItem * > (shape))
        {
            if (name == "width")
            {
               QRectF r = e->rect ();
               r.setWidth (value.toInt ());
               e->setRect (r);
            }
            if (name == "height")
            {
               QRectF r = e->rect ();
               r.setHeight (value.toInt ());
               e->setRect (r);
            }
        }

        if (QGraphicsEllipseItem * e = dynamic_cast < QGraphicsEllipseItem * > (shape))
        {
           if (name == "width")
           {
              QRectF r = e->rect ();
              r.setWidth (value.toInt ());
              e->setRect (r);
           }
           if (name == "height")
           {
              QRectF r = e->rect ();
              r.setHeight (value.toInt ());
              e->setRect (r);
           }
        }
    }

    if (QGraphicsLineItem * e = dynamic_cast < QGraphicsLineItem * > (item))
    {
       if (name == "pen")
       {
           QColor color = value.value <QColor> ();
           e->setPen (color);
       }
       if (name == "width")
       {
          QLineF r = e->line ();
          int w = value.toInt ();
          e->setLine (0, 0, w, r.y2 ());
       }
       if (name == "height")
       {
          QLineF r = e->line ();
          int h = value.toInt ();
          e->setLine (0, 0, r.x2 (), h);
       }
    }
}

/* ---------------------------------------------------------------------- */

void PropertyTable::onItemChanged (QTableWidgetItem * cell)
{
    int line = cell->row ();
    int col = cell->column ();
    if (col == 1)
    {
        QString name = item (line, 0)->text ();
        QVariant value = cell->text ();
        storeProperty (graphics_item, name, value);
        if (name == "tooltip")
        {
            win->refreshTreeName (graphics_item, value.toString ());
        }
        if (name == "pen" || name == "brush")
        {
            cell->setData (Qt::DecorationRole, value.value <QColor> ());
        }

    }
}

/* ---------------------------------------------------------------------- */

QWidget * CustomDelegate::createEditor (QWidget * parent,
                                        const QStyleOptionViewItem & option,
                                        const QModelIndex & index) const
{
   CustomEditor * editor = new CustomEditor (parent);

   if (index.column () == 1)
   {
      QVariant value = index.data (Qt::EditRole);
      QVariant::Type type = value.type ();

      if (type == QVariant::Bool || index.data(Qt::UserRole).toString () == "boolean")
      {
          editor->enable_checkbox = true;
      }
      else if (type == QVariant::Int)
      {
          editor->enable_numeric = true;
      }
      else if (type == QVariant::Double)
      {
          editor->enable_real = true;
      }
      else if (type == QVariant::String)
      {
          editor->enable_text = true;
      }
      else if (type == QVariant::Bool)
      {
          editor->enable_checkbox = true;
      }
      else if (type == QVariant::StringList)
      {
          editor->enable_list = true;
          editor->list_values = value.toStringList ();
      }
      else if (type == QVariant::Color)
      {
         editor->enable_dialog = true;
      }

      QHBoxLayout * layout = new QHBoxLayout (editor);
      layout->setMargin (0);
      layout->setSpacing (0);
      editor->setLayout (layout);

      if (editor->enable_list)
      {
         editor->combo_box = new QComboBox (editor);
         layout->addWidget (editor->combo_box);

         int cnt = editor->list_values.count ();
         for (int inx = 0; inx < cnt; inx++)
            editor->combo_box->addItem (editor->list_values [inx]);
         if (cnt > 0)
            editor->combo_box->setCurrentIndex (0);
      }

      if (editor->enable_checkbox)
      {
         editor->check_box = new QCheckBox (editor);
         layout->addWidget (editor->check_box);
      }

      if (editor->enable_text)
      {
         editor->line_edit = new QLineEdit (editor);
         layout->addWidget (editor->line_edit);
      }

      if (editor->enable_numeric)
      {
          editor->numeric_edit = new QSpinBox (editor);
          editor->numeric_edit->setMaximum (1000);
          layout->addWidget (editor->numeric_edit);
      }

      if (editor->enable_real)
      {
         editor->real_edit = new QDoubleSpinBox (editor);
         layout->addWidget (editor->real_edit);
      }

      if (editor->enable_dialog)
      {
         editor->button = new QPushButton (editor);
         editor->button->setText ("...");
         editor->button->setMaximumWidth (32);
         layout->addWidget (editor->button);

         connect (editor->button, &QPushButton::clicked, editor, &CustomEditor::onDialogClick);
      }
   }

   return editor;
}

void CustomDelegate::setEditorData (QWidget * param_editor,
                                    const QModelIndex & index) const
{
   CustomEditor * editor = dynamic_cast < CustomEditor * > (param_editor);
   editor->cell = table->item (index.row(), index.column ());

   QVariant value = index.data (Qt::EditRole);

   if (editor->check_box != nullptr)
   {
      editor->check_box->setChecked (value.toBool ());
   }

   if (editor->line_edit != nullptr)
   {
      editor->line_edit->setText (value.toString ());
   }

   if (editor->numeric_edit != nullptr)
   {
      editor->numeric_edit->setValue (value.toInt ());
   }

   if (editor->real_edit != nullptr)
   {
      editor->real_edit->setValue (value.toDouble ());
   }
}

void CustomDelegate::setModelData (QWidget * param_editor,
                                   QAbstractItemModel * model,
                                   const QModelIndex & index) const
{
   CustomEditor * editor = dynamic_cast < CustomEditor * > (param_editor);
   // assert (editor != nullptr);

   if (editor->check_box != nullptr)
   {
      bool val = editor->check_box->isChecked ();
      model->setData (index, val);
   }

   if (editor->line_edit != nullptr)
   {
      QString txt = editor->line_edit->text ();
      model->setData (index, txt);
   }

   if (editor->numeric_edit != nullptr)
   {
      int val = editor->numeric_edit->value ();
      model->setData (index, val);
   }

   if (editor->real_edit != nullptr)
   {
      double val = editor->real_edit->value ();
      model->setData (index, val);
   }
}

/*
void CustomDelegate::updateEditorGeometry (QWidget * param_editor,
                                           const QStyleOptionViewItem & option,
                                           const QModelIndex &index) const
{
    QStyledItemDelegate::updateEditorGeometry (param_editor, option, index);
    // param_editor->setGeometry (param_editor->geometry().adjusted(0, 0, -1, -1));
}

QSize CustomDelegate::sizeHint (const QStyleOptionViewItem & option,
                               const QModelIndex & index) const
{
    return QStyledItemDelegate::sizeHint (option, index) + QSize (16, 16);
}
*/

void CustomEditor::onDialogClick ()
{
    QVariant value = cell->data (Qt::EditRole);
    QColor color = value.value <QColor> ();
    color = QColorDialog::getColor (color);
    if (color.isValid ())
    {
        // cell->setData (Qt::DisplayRole, color);
        cell->setData (Qt::EditRole, color);
    }
}
