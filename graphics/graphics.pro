QT       += core gui
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = graphics.bin

CONFIG += CHART
CONFIG += VISUAL
CONFIG += DB
# CONFIG += JS
CONFIG += DBUS
# CONFIG += WEBKIT
# CONFIG += WEBENGINE # either WEBKIT or WEBENGINE
# CONFIG += SVG
# CONFIG += SOCKET
# when CONFIG is modified, menu Build / Clean All

CHART {
    # left braces on the same line as condition
    QT += charts
    DEFINES += CHART
    SOURCES += chart.cc
    HEADERS += chart.h
    # dnf install qt5-qtcharts-devel
}

VISUAL {
    QT += datavisualization
    DEFINES += VISUAL
    HEADERS += visual.h
    SOURCES += visual.cc
    # dnf install qt5-qtdatavis3d-devel
}

DB {
    QT += sql
    DEFINES += DB
    HEADERS += db.h
    SOURCES += db.cc
}

JS {
    QT += script scripttools
    DEFINES += JS
    HEADERS += js.h
    SOURCES += js.cc
    # dnf install qt5-qtscript-devel
}

DBUS {
    QT += dbus
    DEFINES += DBUS
    HEADERS += dbus.h
    SOURCES += dbus.cc
}

SOCKET {
    DEFINES += SOCKET
    HEADERS += socket.h
    SOURCES += socket.cc
}

WEBKIT {
    QT += webkit
    greaterThan(QT_MAJOR_VERSION, 4): QT += webkitwidgets
    DEFINES += HTML WEBKIT
    HEADERS += html.h
    SOURCES += html.cc
    # dnf install qt5-qtwebkit-devel
}

WEBENGINE {
    QT += webengine webenginewidgets
    DEFINES += HTML
    HEADERS += html.h
    SOURCES += html.cc
    # dnf install qt5-qtwebengine-devel
}

SVG {
    QT += svg network
    HEADERS += svg.h
    SOURCES += svg.cc
    DEFINES += SVG
    # dnf install qt5-qtsvg-devel
}

DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += \
    demo.cc \
    colorbutton.cc \
    toolbutton.cc \
    property.cc \
    tree.cc \
    scene.cc \
    io.cc \
    xmlio.cc \
    jsonio.cc

HEADERS += \
    demo.h \
    colorbutton.h \
    toolbutton.h \
    property.h \
    scene.h \
    tree.h \
    win.h \
    io.h \
    xmlio.h \
    jsonio.h

FORMS += \
    demo.ui

RESOURCES += \
    resources.qrc

DISTFILES += \
    data/web.py
