
#include "chart.h"

// add QT += charts to .pro file
// #include <QChart>
// using namespace QtCharts;

#include <QtCharts/QChartView>
#include <QtCharts/QBarSeries>
#include <QtCharts/QBarSet>
#include <QtCharts/QLegend>
#include <QtCharts/QBarCategoryAxis>
#include <QtCharts/QValueAxis>
#include <QtCharts/QPieSeries>
#include <QtCharts/QPieSlice>

QT_CHARTS_USE_NAMESPACE

/* ---------------------------------------------------------------------- */

QWidget * chartExample (QGraphicsScene * scene)
{
    // dnf install qt5-qtcharts-devel
    // QT += charts in .pro file
    // Build / Clean All
    // #include <QChart>

    // QChart * chart = new QChart;

    QStringList categories;
    QBarSet * xset = new QBarSet ("X");
    QBarSet * yset = new QBarSet ("Y");

    for (QGraphicsItem * item : scene->items (Qt::AscendingOrder))
    {
        categories << item->toolTip ();
        *xset << item->x();
        *yset << item->y();
    }

    QBarSeries * series = new QBarSeries ();
    series->append (xset);
    series->append (yset);

    QChart * chart = new QChart ();
    chart->addSeries (series);
    chart->setTitle ("Simple chart example");

    QBarCategoryAxis * axisX = new QBarCategoryAxis ();
    axisX->append (categories);
    chart->addAxis (axisX, Qt::AlignBottom);
    series->attachAxis (axisX);

    QValueAxis * axisY = new QValueAxis ();
    axisY->setRange (0, 200);
    chart->addAxis (axisY, Qt::AlignLeft);
    series->attachAxis (axisY);

    chart->legend()->setVisible (true);
    chart->legend()->setAlignment (Qt::AlignBottom);

    QChartView * view = new QChartView (chart);
    return view;

    // chart->setParent (this);
    // chart->setToolTip ("chart");
    // chart->resize (400, 200);
    // chart->setBackgroundBrush (QColor ("silver").light(120));
    // chart->setFlags (QGraphicsItem::ItemIsMovable | QGraphicsEllipseItem::ItemIsSelectable);
    // chart->setData (opaqueKey, true); // do not display tree subitems
    // scene->addItem (chart);
    // refreshTree ();
}

/* ---------------------------------------------------------------------- */

QWidget * pieChartExample (QGraphicsScene * scene)
{
    QPieSeries *series = new QPieSeries();
    for (QGraphicsItem * item : scene->items (Qt::AscendingOrder))
    {
        series->append (item->toolTip (), item->x());
    }

    QChart *chart = new QChart();
    chart->addSeries (series);
    // chart->setTitle ("Simple piechart example");
    chart->legend()->hide();

    QChartView * view = new QChartView (chart);
    return view;

    // chart->setToolTip ("pie chart");
    // chart->resize (320, 320);
    // chart->setBackgroundBrush (QColor ("slateblue"));
    // chart->setFlags (QGraphicsItem::ItemIsMovable | QGraphicsEllipseItem::ItemIsSelectable);
    // chart->setData (opaqueKey, true); // do not display tree subitems
    // scene->addItem (chart);
    // refreshTree ();
}

/* ---------------------------------------------------------------------- */

