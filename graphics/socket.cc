
/* socket.cc */

#include "socket.h"

#include <QTcpSocket>
// #include <iostream>

SocketReceiver::SocketReceiver (QWidget * parent, int port_param, QTextEdit * info_param) :
   QObject (parent),
   port (port_param),
   info (info_param)
{
   run ();
}

SocketReceiver::~SocketReceiver()
{
    // std::cerr << "SocketReciever destroyed" << std::endl;
}

void SocketReceiver::print (QString s)
{
   if (info != nullptr)
      info->append (s);
}

void SocketReceiver::connection ()
{
    QTcpSocket * socket = server->nextPendingConnection();
    connect (socket, &QTcpSocket::disconnected, socket, &QObject::deleteLater);

    // print ("New connection");
    print ("New connection from " + socket->peerName() + " port " + QString::number (socket->peerPort()));

    while (!(socket->waitForReadyRead (100))) { } // <-- important, waiting for data to be read from web browser
    QByteArray request = socket->readAll();
    QList<QByteArray> lines = request.split ('\n');
    QList<QByteArray> words = lines[0].split (' ');

    QByteArray method = words[0];
    QByteArray path = words [1];
    print (method + " " + path);
    // print ("REQUEST\r\n" + request);

    QByteArray block = "";
    block += "HTTP/1.1 200 OK\r\n";

    if (false)
    {
        block += "Content-Type: text/text\r\n";
        block += "\r\n";
        block += "Hello from Qt\r\n";
    }

    if (method == "GET" && path == "/")
    {
        block += "Content-Type: text/html\r\n";
        block += "\r\n";
        block += "<!DOCTYPE html>\r\n";
        block += "<html>\r\n";
        block += "   <head>\r\n";
        block += "      <title>Local</title>\r\n";
        block += "   </head>\r\n";
        block += "   <body>\r\n";
        block += "       Hello from Qt\r\n";
        block += "   </body>\r\n";
        block += "</html>\r\n";
        block += "\r\n";
    }

    socket->write (block);

    socket->flush ();
    socket->waitForBytesWritten (3000);

    socket->disconnectFromHost();
}

void SocketReceiver::run()
{
    server = new QTcpServer (this);
    connect (server, &QTcpServer::newConnection, this, &SocketReceiver::connection);
    bool ok = server->listen (QHostAddress::Any, port);
    if (ok)
       print ("Listening on port " + QString::number (port));
    else
       print ("Error when listening");
}

/* ---------------------------------------------------------------------- */

// http://doc.qt.io/qt-5/qtnetwork-fortuneserver-example.html

// http://www.bogotobogo.com/cplusplus/sockets_server_client.php
// http://www.bogotobogo.com/Qt/Qt5_QTcpServer_Client_Server.php
// http://www.bogotobogo.com/Qt/Qt5_QTcpServer_Multithreaded_Client_Server.php
// http://www.bogotobogo.com/Qt/Qt5_QTcpServer_QThreadPool_Multithreaded_Client_Server.php

// http://stackoverflow.com/questions/3122508/qt-http-server  "answer: Here is a very simple HTTP web server"

// http://github.com/qt-labs/qthttpserver/blob/master/src/httpserver/qabstracthttpserver.cpp

// kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all
