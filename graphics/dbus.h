/* dbus.h */

#ifndef DBUS_H
#define DBUS_H

#include <QObject>
#include <QTextEdit>

class Receiver : public QObject
{
   Q_OBJECT
   Q_CLASSINFO ("D-Bus Interface", "org.example.ReceiverInterface")

   public:
      explicit Receiver (QWidget * parent, QTextEdit * info_param);
      ~Receiver();

   public slots:
       Q_SCRIPTABLE void hello (QString s);

   private:
      QTextEdit * info;
};

#endif // DBUS_H
