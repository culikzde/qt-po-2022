QT += core gui widgets qml
# dnf install qt5-qtquickcontrols2-devel

CONFIG += c++11 precompile_header

PRECOMPILED_HEADER += precompiled.h

SOURCES += script.cc \
    box.cc

HEADERS += script.h precompiled.h \
    box.h

FORMS += \
    script.ui
