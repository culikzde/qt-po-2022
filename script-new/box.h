#ifndef BOX_H
#define BOX_H

#include "precompiled.h"

class Box : public QObject, public QGraphicsRectItem
{
    Q_OBJECT
public:
    Box ();
    QString name;
    Q_PROPERTY(QString name MEMBER name SCRIPTABLE true)
    Q_SCRIPTABLE void move (int x, int y);
    Q_SCRIPTABLE void setColor (QString color);
    Q_SCRIPTABLE void setBorder (QString color, int width = 1);
};

#endif // BOX_H
