#ifndef IO_H
#define IO_H

#include "precompiled.h"

const QString shapeFormat = "application/x-shape";

/* Custom Table Editor */

class FieldDesc;

class TableItem : public QTableWidgetItem
{
public:
    QGraphicsItem * graph_item = nullptr;
    FieldDesc * field;
};

class CustomEditor : public QWidget
{
public:
      bool enable_checkbox;
      bool enable_list;
      bool enable_text;
      bool enable_numeric;
      bool enable_real;
      bool enable_dialog;

      QStringList list_values;

      QCheckBox * check_box;
      QLineEdit * line_edit;
      QSpinBox * numeric_edit;
      QDoubleSpinBox * real_edit;
      QComboBox * combo_box;
      QPushButton * button;

      TableItem * table_item;

      void onDialogClick ();

      CustomEditor (QWidget * parent) :
            QWidget (parent),
            enable_checkbox (false),
            enable_list (false),
            enable_text (false),
            enable_numeric (false),
            enable_real (false),
            enable_dialog (false),

            check_box (nullptr),
            line_edit (nullptr),
            numeric_edit (nullptr),
            real_edit (nullptr),
            combo_box (nullptr),
            button (nullptr),

            table_item (nullptr)
      { }
};

/* Field Description */

class FieldDesc
{
public:
    QString name;
    virtual QVariant read (QGraphicsItem * item) { return QVariant (); }
    virtual void write (QGraphicsItem * item, QVariant value) { }

    virtual QString readText (QGraphicsItem * item) { return read (item).toString (); }
    virtual QVariant decoration (QGraphicsItem * item) { return QVariant (); }
    virtual void setupEditor (CustomEditor * editor) { }
};

class BoolFieldDesc : public FieldDesc
{
public:
    void setupEditor (CustomEditor * editor) override { editor->enable_checkbox = true; }
};

class IntFieldDesc : public FieldDesc
{
public:
    void setupEditor (CustomEditor * editor) override { editor->enable_numeric = true; }
};

class DoubleFieldDesc : public FieldDesc
{
public:
    void setupEditor (CustomEditor * editor) override { editor->enable_real = true; }
};

class StringFieldDesc : public FieldDesc
{
public:
    void setupEditor (CustomEditor * editor) override { editor->enable_text = true; }
};

class ColorFieldDesc : public FieldDesc
{
public:
    virtual QString readText (QGraphicsItem * item) override;
    virtual QVariant decoration (QGraphicsItem * item) override { return read (item); }
    void setupEditor (CustomEditor * editor) override { editor->enable_dialog = true; }
};

/* Type Description */

class TypeDesc
{
public:
    QString name;
    QList < FieldDesc * > fields;
    void add (FieldDesc * field) { fields.append (field); }
    virtual QGraphicsItem * createItem () { return nullptr; }
    FieldDesc * findField (QString field_name);
};

/* Graphic Item Fields */

class TooltipField : public StringFieldDesc
{
public:
    TooltipField () { name = "toolTip"; }

    QVariant read (QGraphicsItem * item) override
    {
        return item->toolTip ();
    }

    void write (QGraphicsItem * item, QVariant value) override
    {
        item->setToolTip (value.toString ());
    }
};

class XField : public DoubleFieldDesc
{
public:
    typedef QAbstractGraphicsShapeItem T;

    XField () { name = "x"; }

    QVariant read (QGraphicsItem * item) override
    {
        T * t = dynamic_cast <T*> (item);
        return t->pos().x();
    }

    void write (QGraphicsItem * item, QVariant value) override
    {
        T * t = dynamic_cast <T*> (item);
        QPointF point = t->pos ();
        point.setX (value.toDouble ());
        t->setPos (point);
    }
};

class YField : public DoubleFieldDesc
{
public:
    typedef QAbstractGraphicsShapeItem T;

    YField () { name = "y"; }

    QVariant read (QGraphicsItem * item) override
    {
        T * t = dynamic_cast <T*> (item);
        return t->pos().y();
    }

    void write (QGraphicsItem * item, QVariant value) override
    {
        T * t = dynamic_cast <T*> (item);
        QPointF point = t->pos ();
        point.setY (value.toDouble ());
        t->setPos (point);
    }
};

template <class T>
class WidthField : public DoubleFieldDesc
{
public:
    WidthField () { name = "width"; }

    QVariant read (QGraphicsItem * item) override
    {
        T * t = dynamic_cast <T*> (item);
        return t->rect ().width ();
    }

    void write (QGraphicsItem * item, QVariant value) override
    {
        T * t = dynamic_cast <T*> (item);
        QRectF box = t->rect ();
        box.setWidth (value.toDouble ());
        t->setRect (box);
    }
};

template <class T>
class HeightField : public DoubleFieldDesc
{
public:
    HeightField () { name = "height"; }

    QVariant read (QGraphicsItem * item) override
    {
        T * t = dynamic_cast <T*> (item);
        return t->rect ().height ();
    }

    void write (QGraphicsItem * item, QVariant value) override
    {
        T * t = dynamic_cast <T*> (item);
        QRectF box = t->rect ();
        box.setHeight (value.toDouble ());
        t->setRect (box);
    }
};

template <class T>
class PenField : public ColorFieldDesc
{
public:
    PenField () { name = "pen"; }

    QVariant read (QGraphicsItem * item) override
    {
        T * t = dynamic_cast <T*> (item);
        return t->pen ().color ();
    }

    void write (QGraphicsItem * item, QVariant value) override
    {
        T * t = dynamic_cast <T*> (item);
        QColor color = value.value <QColor> ();
        QPen pen = t->pen ();
        pen.setColor (color);
        t->setPen (pen);
    }
};

template <class T>
class PenWidthField : public IntFieldDesc
{
public:
    PenWidthField () { name = "border"; }

    QVariant read (QGraphicsItem * item) override
    {
        T * t = dynamic_cast <T*> (item);
        return t->pen ().width ();
    }

    void write (QGraphicsItem * item, QVariant value) override
    {
        T * t = dynamic_cast <T*> (item);
        QPen pen = t->pen ();
        pen.setWidth (value.toInt ());
        t->setPen (pen);
    }
};

template <class T>
class BrushField : public ColorFieldDesc
{
public:
    BrushField () { name = "brush"; }

    QVariant read (QGraphicsItem * item) override
    {
        T * t = dynamic_cast <T*> (item);
        return t->brush ().color ();
    }

    void write (QGraphicsItem * item, QVariant value) override
    {
        T * t = dynamic_cast <T*> (item);
        QColor color = value.value <QColor> ();
        QBrush brush = t->brush ();
        brush.setColor (color);
        t->setBrush (brush);
    }
};

#define LINE_FIELD(type_name, field_name, param1, param2, param3, param4) \
class type_name : public DoubleFieldDesc \
{ \
public: \
    typedef QGraphicsLineItem T; \
 \
    type_name () { name = #field_name; } \
 \
    QVariant read (QGraphicsItem * item) override \
    { \
        T * t = dynamic_cast <T*> (item); \
        return t->line ().field_name (); \
    } \
 \
    void write (QGraphicsItem * item, QVariant value) override \
    { \
        T * t = dynamic_cast <T*> (item); \
        qreal num = value.toDouble (); \
        QLineF box = t->line (); \
        box.setLine (param1, param2, param3, param4); \
        t->setLine (box); \
    } \
};

LINE_FIELD (X1Field, x1, num, box.y1(), box.x2(), box.y2())
LINE_FIELD (Y1Field, y1, box.x1(), num, box.x2(), box.y2())
LINE_FIELD (X2Field, x2, box.x1(), box.y1(), num, box.y2())
LINE_FIELD (Y2Field, y2, box.x1(), box.y1(), box.x2(), num)

/* Graphics Item Type Descriptions */

class GraphicsItemTypeDesc : public TypeDesc
{
public:
    GraphicsItemTypeDesc ()
    {
        name = "item";
        add (new TooltipField);
    }
};

class RectangleTypeDesc : public TypeDesc
{
public:
    RectangleTypeDesc ()
    {
        name = "rectangle";
        add (new TooltipField);
        add (new XField);
        add (new YField);
        typedef QGraphicsRectItem T;
        add (new WidthField <T>);
        add (new HeightField <T>);
        add (new PenWidthField <T>);
        add (new PenField <T>);
        add (new BrushField <T>);
    }

    QGraphicsItem * createItem () override
    {
        QGraphicsRectItem * item = new QGraphicsRectItem;
        item->setRect (0, 0, 100, 80);
        item->setPen (QColor ("blue"));
        item->setBrush (QColor ("yellow")); // solid brush required
        item->setToolTip (name);
        item->setFlags (QGraphicsItem::ItemIsMovable | QGraphicsEllipseItem::ItemIsSelectable);
        return item;
    }
};

class EllipseTypeDesc : public TypeDesc
{
public:
    EllipseTypeDesc ()
    {
        name = "ellipse";
        add (new TooltipField);
        add (new XField);
        add (new YField);
        typedef QGraphicsEllipseItem T;
        add (new WidthField <T>);
        add (new HeightField <T>);
        add (new PenWidthField <T>);
        add (new PenField <T>);
        add (new BrushField <T>);
    }

    QGraphicsItem * createItem () override
    {
        QGraphicsEllipseItem * item = new QGraphicsEllipseItem;
        item->setRect (0, 0, 60, 40);
        item->setPen (QColor ("orange"));
        item->setBrush (QColor ("lime")); // solid brush required
        item->setToolTip (name);
        item->setFlags (QGraphicsItem::ItemIsMovable | QGraphicsEllipseItem::ItemIsSelectable);
        return item;
    }
};

class LineTypeDesc : public TypeDesc
{
public:
    LineTypeDesc ()
    {
        name = "line";
        add (new TooltipField);
        add (new X1Field);
        add (new Y1Field);
        add (new X2Field);
        add (new Y2Field);
        typedef QGraphicsLineItem T;
        add (new PenWidthField <T>);
        add (new PenField <T>);
    }

    QGraphicsItem * createItem () override
    {
        QGraphicsLineItem * item = new QGraphicsLineItem;
        item->setLine (0, 0, 60, 40);
        item->setPen (QPen (QColor ("red"), 5));
        item->setToolTip (name);
        item->setFlags (QGraphicsItem::ItemIsMovable | QGraphicsEllipseItem::ItemIsSelectable);
        return item;
    }
};

static GraphicsItemTypeDesc graphicsItemProperties;
static RectangleTypeDesc rectangleProperties;
static EllipseTypeDesc ellipseProperties;
static LineTypeDesc lineProperties;

TypeDesc * typeDescription (QGraphicsItem * item);
TypeDesc * findDescription (QString name);

/* Color Names */

static QMap < QString, QString > colorMap;
void initColorMap ();

/* JSON IO */

void readJson (QByteArray code, QGraphicsScene * scene,  QGraphicsItem * target = nullptr);
QByteArray writeJson (QGraphicsScene * scene);

/* XML IO */

void readXml (QXmlStreamReader & r, QGraphicsScene * scene, QGraphicsItem * target = nullptr);
void writeXml (QXmlStreamWriter & w, QGraphicsScene * scene);

void writeXmlItem (QXmlStreamWriter & w, QGraphicsItem * item);

#endif // IO_H
