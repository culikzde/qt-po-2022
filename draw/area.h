#ifndef AREA_H
#define AREA_H

#include "precompiled.h"

class Target;

class Source : public QGraphicsRectItem
{
public:
    Source ();
    QGraphicsLineItem * line = nullptr;
    Target * target = nullptr;
    void updateLine ();
protected:
    QVariant itemChange (GraphicsItemChange change, const QVariant &value) override;
};

class Target : public QGraphicsRectItem
{
public:
    Target ();
    Source * source = nullptr;

protected:
    QVariant itemChange (GraphicsItemChange change, const QVariant &value) override;
    void contextMenuEvent (QGraphicsSceneContextMenuEvent * event) override;

private:
    bool stop = false;
};

#endif // AREA_H
