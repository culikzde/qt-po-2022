#ifndef TOOLBUTTON_H
#define TOOLBUTTON_H

#include "precompiled.h"

const QString toolFormat = "application/x-tool";

class ToolButton : public QToolButton
{
public:
    ToolButton();
private:
   QString name;
   QIcon icon;
protected:
    void mousePressEvent (QMouseEvent *event);
public:
    ToolButton (QString p_name, QString p_icon_name = "");
};

void addToolButtons (QToolBar * page);

#endif // TOOLBUTTON_H
