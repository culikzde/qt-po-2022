#include "area.h"

Source::Source()
{
   setRect (0, 0, 8, 8);
   setBrush (QColor ("lime"));
   setZValue (10);
   setFlags (QGraphicsItem::ItemIsMovable |
             QGraphicsItem::ItemIsSelectable |
             QGraphicsItem::ItemSendsScenePositionChanges);

   line = new QGraphicsLineItem;
   line->setPen (QColor ("red"));
   line->setParentItem (this);

   target = new Target ();
   target->source = this;
   target->setPos (100, 100);
   target->setParentItem (this);

   updateLine ();
}

Target::Target()
{
   setRect (0, 0, 8, 8);
   setBrush (QColor ("orange"));
   setZValue (10);
   setFlags (QGraphicsItem::ItemIsMovable |
             QGraphicsItem::ItemIsSelectable |
             QGraphicsItem::ItemSendsGeometryChanges |
             QGraphicsItem::ItemSendsScenePositionChanges);
}

QVariant Source::itemChange (GraphicsItemChange change, const QVariant &value)
{
    if (change == QGraphicsItem::ItemScenePositionHasChanged)
       updateLine ();

    return QGraphicsRectItem::itemChange (change, value);
}

QVariant Target::itemChange (GraphicsItemChange change, const QVariant &value)
{
    if (change == QGraphicsItem::ItemScenePositionHasChanged)
       if (source != nullptr && ! stop)
           source->updateLine();

    return QGraphicsRectItem::itemChange (change, value);
}

void Source::updateLine ()
{
    if (line != nullptr && target != nullptr)
    {
        QRectF box = rect ();
        QPointF start (box.x() + box.width ()/2,
                       box.y() + box.height()/2);

        box = target->rect ();
        QPointF stop (box.x() + box.width ()/2,
                       box.y() + box.height()/2);

        start = line->mapFromItem (this, start);
        stop = line->mapFromItem (target, stop);

        line->setLine (start.x(), start.y(), stop.x(), stop.y());
    }
}

void Target::contextMenuEvent (QGraphicsSceneContextMenuEvent *event)
{
    QMenu menu;
    QAction * connectAction = menu.addAction ("Connect");
    QAction * selectedAction = menu.exec(event->screenPos());
    if (selectedAction != nullptr)
    {
        QRectF box = rect ();
        QPointF point (box.x() + box.width ()/2,
                       box.y() + box.height()/2);
        point = mapToScene(point);

        QGraphicsItem * result = nullptr;
        QList<QGraphicsItem *> list = scene()->items (point);
        for (QGraphicsItem * item : list)
            if (item != this && result == nullptr)
                result = item;

        if (result != nullptr)
        {
            setBrush (QColor ("red"));
            QPointF pt = QPointF (0, 0);
            pt = result->mapFromItem (this, pt);
            stop = true;
            this->setParentItem (result);
            this->setPos (pt);
            stop = false;
            if (source != nullptr) source->updateLine ();
        }
    }
}
