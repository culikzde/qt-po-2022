#ifndef TREE_H
#define TREE_H

#include "precompiled.h"

class TreeNode : public QTreeWidgetItem
{
public:
    QGraphicsItem * graph_item = nullptr;
};

class Tree : public QTreeWidget
{
public:
    Tree (QWidget * parent = nullptr);

protected:
    QStringList mimeTypes() const override;
    // Qt::DropActions supportedDropActions() const override;
    bool dropMimeData(QTreeWidgetItem * parent, int index, const QMimeData *data, Qt::DropAction action) override;
    // QMimeData *mimeData (const QList<QTreeWidgetItem *> items) const override;
};

#endif // TREE_H
