#include "precompiled.h"
#include "io.h"

/* Type Descriptions */

TypeDesc * typeDescription (QGraphicsItem * item)
{
    TypeDesc * desc = & graphicsItemProperties;

    if (dynamic_cast < QGraphicsRectItem * > (item) != nullptr)
        desc = & rectangleProperties;

    else if (dynamic_cast < QGraphicsEllipseItem * > (item) != nullptr)
        desc = & ellipseProperties;

    else if (dynamic_cast < QGraphicsLineItem * > (item) != nullptr)
        desc = & lineProperties;

    return desc;
}

TypeDesc * findDescription (QString name)
{
    TypeDesc * desc = & graphicsItemProperties;

    if (name == rectangleProperties.name)
        desc = & rectangleProperties;

    else if (name == ellipseProperties.name)
        desc = & ellipseProperties;

    else if (name == lineProperties.name)
        desc = & lineProperties;

    return desc;
}

FieldDesc * TypeDesc::findField (QString field_name)
{
    FieldDesc * result = nullptr;
    for (FieldDesc * field : fields)
        if (field->name == field_name)
            result = field;
    return result;
}

/* Color Names */

void initColorMap ()
{
    QStringList list = QColor::colorNames ();
    for (QString name : list)
    {
        QColor color = QColor (name);
        colorMap [color.name ()] = name;
    }
}

QString ColorFieldDesc::readText (QGraphicsItem * item)
{
    QVariant value = read (item);
    QColor color = value.value <QColor> ();
    QString text = color.name ();
    if (colorMap.contains (text))
       text = colorMap [text];
    return text;
}

/* Read JSON */

void readJsonItems (QJsonObject obj, QGraphicsScene * scene,  QGraphicsItem * target);

void readJsonItem (QJsonObject obj, QGraphicsScene * scene,  QGraphicsItem * target)
{
    QString type_name = obj ["type"].toString ();
    TypeDesc * desc = findDescription (type_name);
    QGraphicsItem * item = desc->createItem ();

    if (item != nullptr)
    {
       for (FieldDesc * field : desc->fields)
       {
           if (obj.contains (field->name))
           {
               QJsonValue value = obj [field->name];
               field->write (item, value.toVariant ());
           }
       }

       readJsonItems (obj, scene, item); // read inner items, add to this item

       item->setFlag (QGraphicsItem::ItemIsMovable);
       item->setFlag (QGraphicsItem::ItemIsSelectable);

       if (target != nullptr)
          item->setParentItem (target);
       else
          scene->addItem (item);
    }
}

void readJsonItems (QJsonObject obj, QGraphicsScene * scene,  QGraphicsItem * target)
{
    if (obj.contains ("items") && obj ["items"].isArray())
    {
        QJsonArray list = obj ["items"].toArray ();
        for (QJsonValue item : list)
        {
            if (item.isObject ())
               readJsonItem (item.toObject (), scene, target);
        }
    }
}

void readJson (QByteArray code, QGraphicsScene * scene,  QGraphicsItem * target)
{
    QJsonDocument doc = QJsonDocument::fromJson (code);
    QJsonObject obj = doc.object ();
    readJsonItems (obj, scene, target);
}

/* Write JSON */

QJsonObject writeJsonItem (QGraphicsItem * item)
{
    QJsonObject obj;
    TypeDesc * desc = typeDescription (item);

    obj ["type"] = desc->name;

    for (FieldDesc * field : desc->fields)
    {
        QString text = field->readText (item);
        obj [field->name] = text;
    }

    QJsonArray list;
    for (QGraphicsItem * t : item->childItems ())
    {
        QJsonObject v = writeJsonItem (t);
        list.append (v);
    }
    obj ["items"] = list;

    return obj;
}

QByteArray writeJson (QGraphicsScene * scene)
{
    QJsonObject obj;
    QJsonArray list;

    for (QGraphicsItem * item : scene->items (Qt::AscendingOrder))
    {
       if (item->parentItem() == nullptr)
          list.append (writeJsonItem (item));
    }

    obj ["items"] = list;

    QJsonDocument doc (obj);
    QByteArray code = doc.toJson ();
    return code;
}

/* Read XML */

QGraphicsItem * readXmlItem (QXmlStreamReader & r)
{
    QString type_name = r.name().toString();
    TypeDesc * desc = findDescription (type_name);
    assert (desc != nullptr);

    QGraphicsItem * item = desc->createItem ();
    if (item != nullptr)
    {
        QXmlStreamAttributes a = r.attributes();

        for (FieldDesc * field : desc->fields)
        {
            if (a.hasAttribute (field->name))
            {
                QString value = a.value (field->name).toString();
                field->write (item, value);
            }
        }

        item->setFlag (QGraphicsItem::ItemIsMovable);
        item->setFlag (QGraphicsItem::ItemIsSelectable);
    }

    return item;
}

void readXml (QXmlStreamReader & r, QGraphicsScene * scene, QGraphicsItem * target)
{
    while (! r.atEnd())
    {
        if (r.isStartElement())
        {
            QString type = r.name().toString();
            if (type != "data") // top level element
            {
                QGraphicsItem * item = readXmlItem (r);
                if (item != nullptr)
                {
                    // add item
                    if (target != nullptr)
                       item->setParentItem (target);
                    else
                       scene->addItem (item);

                    // change target
                    target = item;
                }
            }
        }
        else if (r.isEndElement())
        {
            if (target != nullptr)
                target = target->parentItem (); // back to above target
        }
        r.readNext();
    }
}

/* Write XML */

void writeXmlItem (QXmlStreamWriter & w, QGraphicsItem * item)
{
    TypeDesc * desc = typeDescription (item);

    w.writeStartElement (desc->name);

    for (FieldDesc * field : desc->fields)
         w.writeAttribute (field->name, field->readText (item));

    for (QGraphicsItem * t : item->childItems ())
        writeXmlItem (w, t);

    w.writeEndElement();
}

void writeXml (QXmlStreamWriter & w, QGraphicsScene * scene)
{
    w.setAutoFormatting (true);
    w.writeStartDocument ();
    w.writeStartElement ("data");

    for (QGraphicsItem * item : scene->items (Qt::AscendingOrder))
    {
       if (item->parentItem() == nullptr)
          writeXmlItem (w, item);
    }

    w.writeEndElement(); // end of data
    w.writeEndDocument();
}

