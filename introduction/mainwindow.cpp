#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::print (QString s)
{
    ui->textEdit->append (s);
}

void MainWindow::setColor (QColor c)
{
    QTextCharFormat fmt = ui->textEdit->currentCharFormat();
    fmt.setForeground (c);
    ui->textEdit->setCurrentCharFormat (fmt);
}

void MainWindow::on_pushButton_clicked()
{
    setColor (QColor::fromRgb(0, 0, 255));

    QString a = ui->lineEdit->text();
    print ("line edit ... " + a);

    setColor (QColor::fromRgb(255, 0, 0));

    bool b = ui->checkBox->isChecked();
    QString s = "vypnuto";
    if (b)
        s = "zapnuto";
    print ("check box ... " + s);

    setColor ("lime");
    int c = ui->spinBox->value();

    QTextCharFormat fmt = ui->textEdit->currentCharFormat();
    fmt.setToolTip ("vysledek");
    ui->textEdit->setCurrentCharFormat (fmt);

    print ("spin box ... " + QString::number (c));
    ui->spinBox->setValue (c+10);


}
