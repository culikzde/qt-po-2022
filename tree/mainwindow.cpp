#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFont>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QIcon::setThemeName ("mate");
    // setFont (QFont ("Sans Serif", 16));
}

void MainWindow::on_pushButton_clicked()
{
    QString s = ui->lineEdit->text ();
    QTreeWidgetItem * item = new QTreeWidgetItem;
    // auto item = new QTreeWidgetItem;
    ui->treeWidget->addTopLevelItem (item);
    item->setText (0, s);
    item->setForeground (0, QColor ("red"));
    item->setIcon (0, QIcon::fromTheme("folder"));

    int n = ui->spinBox->value();
    for (int i = 1; i <= n; ++i)
    {
        QTreeWidgetItem * node = new QTreeWidgetItem;
        node->setText (0, s + " " + QString::number (i));
        node->setForeground (0, QColor::fromHsv (360/n*(i-1), 240, 240));

        item->addChild (node);
    }
    item->setExpanded (true);
}

MainWindow::~MainWindow()
{
    delete ui;
}
