#include "colorbutton.h"

#include <QDrag>
#include <QMimeData>
#include <QMouseEvent>
#include <QPainter>

ColorButton::ColorButton (QToolBar * parent, QColor p_color, QString p_name) :
   QToolButton (parent)
{
    color = p_color;
    name = p_name;

    setToolTip (name);
    setIcon (getPixmap ());

    parent->addWidget (this);
}

QPixmap ColorButton::getPixmap()
{
    const int w = 16; /* 12, 24, 48 */
    QPixmap pixmap (w, w);

    pixmap.fill (Qt::transparent);

    QPainter painter (&pixmap);
    painter.setPen (color);
    painter.setBrush (color);
    painter.drawEllipse (1, 1, w-2, w-2);
    painter.end ();

    return pixmap;
}

void ColorButton::mousePressEvent (QMouseEvent * event)
{
    if (event->button() == Qt::LeftButton )
    {
       QMimeData * mimeData = new QMimeData;
       mimeData->setColorData (color);

       QDrag * drag = new QDrag (this);
       drag->setMimeData (mimeData);
       drag->setPixmap (getPixmap ());
       drag->setHotSpot (QPoint (-16, -16));

       Qt::DropAction dropAction = drag->exec (Qt::MoveAction | Qt::CopyAction | Qt::LinkAction);
    }
}

void addColorButtons (QToolBar * page)
{
    QStringList names;

    names << "red" << "green" << "blue" << "yellow"
          << "orange" << "silver" << "gold" << "goldenrod"
          << "lime" << "lime green" << "yellow green" << "green yellow"
          << "forest green" << "coral" << "cornflower blue" << "dodger blue"
          << "royal blue" << "wheat" << "chocolate" << "peru" << "sienna" << "brown";

    for (QString name : names)
    {
       new ColorButton (page, QColor (name), name);
    }
}
