#!/usr/bin/env python

import sys

from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtDBus import *

import dbus
import dbus.service

# --------------------------------------------------------------------------

class Win (QTextEdit) :

    Q_CLASSINFO ("D-Bus Interface", "org.example.ReceiverInterface")

    def __init__ (self, parent=None) :
        super (Win, self).__init__ (parent)

        connection = QDBusConnection.sessionBus()
        connection.registerService ("org.example.receiver")
        connection.registerObject("/org/example/ReceiverObject", self, QDBusConnection.ExportAllSlots)

    @pyqtSlot(str, result=str)
    def hello (self, hello_message):
        self.append ("Hello called with parameter: " + str (hello_message))
        return "Hello from qlisten (" + hello_message + ")"

# --------------------------------------------------------------------------

if __name__ == '__main__' :
   app = QApplication (sys.argv)
   win = Win ()
   win.show ()
   app.exec_ ()

# --------------------------------------------------------------------------

# dnf install python3-qt5

# kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all
