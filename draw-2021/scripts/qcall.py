#!/usr/bin/env python

import sys

from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtDBus import *

import dbus

# --------------------------------------------------------------------------

class Win (QWidget) :

    def __init__ (self, parent=None) :
        super (Win, self).__init__ (parent)

        button = QPushButton ()
        button.setText ("call hello")

        button2 = QPushButton ()
        button2.setText ("another method")

        view = QTextEdit ()

        layout = QVBoxLayout ()
        layout.addWidget (button)
        layout.addWidget (button2)
        layout.addWidget (view)
        self.setLayout (layout)

        self.view = view

        button.clicked.connect (self.callHello)
        button2.clicked.connect (self.anotherMethod)

    def callHello (self) :
        connection = QDBusConnection.sessionBus()
        iface = QDBusInterface("org.example.receiver", "/org/example/ReceiverObject", '',
                connection)

        msg = iface.call ("hello", "Hello from Qt")
        reply = QDBusReply(msg)

        if reply.isValid():
            self.view.append ("Reply was: " + str (reply.value()))
        else :
            self.view.append ("Call failed: " + reply.error().message())

    def anotherMethod (self) :
        bus = dbus.SessionBus ()
        remote_object = bus.get_object ("org.example.receiver", "/org/example/ReceiverObject")
        ifc = dbus.Interface (remote_object, "org.example.ReceiverInterface")
        answer = ifc.hello ("Hello form script")
        self.view.append ("answer is: " + str (answer))

# --------------------------------------------------------------------------

if __name__ == '__main__' :
   app = QApplication (sys.argv)
   win = Win ()
   win.show ()
   app.exec_ ()

# --------------------------------------------------------------------------

# kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all
