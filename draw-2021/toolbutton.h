#ifndef TOOLBUTTON_H
#define TOOLBUTTON_H

#include <QToolButton>
#include <QToolBar>
#include <QIcon>

const QString toolFormat = "application/x-tool";
const QString widgetFormat = "application/x-widget";

class ToolButton : public QToolButton
{
private:
   QString name;
   QIcon icon;
   QString format;
protected:
    void mousePressEvent (QMouseEvent *event);
public:
    ToolButton (QToolBar * parent, QString p_name, QString p_icon_name = "", QString p_format = toolFormat);
};

void addToolButton (QToolBar * page, QString name, QString icon = "", QString format = toolFormat);
void addToolButtons (QToolBar * page);

#endif // TOOLBUTTON_H
